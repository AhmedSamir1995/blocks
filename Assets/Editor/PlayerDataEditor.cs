﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LeaderBoardAsset))]
[CanEditMultipleObjects]
public class CardsDeckEditor : Editor
{
   /* LeaderBoardAsset lb;

    private void OnEnable()
    {
    }

    public override void OnInspectorGUI()
    {
        lb = (LeaderBoardAsset)target;
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update();

        // Show the custom GUI controls.

        EditorGUILayout.BeginHorizontal();

        //EditorGUILayout.LabelField("First Name", EditorStyles.boldLabel, GUILayout.ExpandWidth(true));
        //EditorGUILayout.LabelField("Last Name", EditorStyles.boldLabel, GUILayout.ExpandWidth(true));
        //EditorGUILayout.LabelField("Score", EditorStyles.boldLabel, GUILayout.ExpandWidth(true));

        EditorGUILayout.TextField("First Name"  , EditorStyles.boldLabel);
        EditorGUILayout.TextField("Last Name"   , EditorStyles.boldLabel);
        EditorGUILayout.TextField("Score"       , EditorStyles.boldLabel);
        EditorGUILayout.EndHorizontal();
        
        for (int i = 0; i < lb.playersData.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.TextField(lb.playersData[i].firstName);
            EditorGUILayout.TextField(lb.playersData[i].lastName);
            EditorGUILayout.TextField(lb.playersData[i].score.ToString());
            if (GUILayout.Button("-"))
            {
                lb.playersData.RemoveAt(i);
                
            }
            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("+"))
        {
            lb.playersData.Add(new PlayerData() { firstName = "First Name", lastName = "Last Name", score = 0 });

        }

    }*/
}
