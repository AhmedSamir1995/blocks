﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    public UnityEngine.Events.UnityEvent onRowComplete;
    public UnityEngine.Events.UnityEvent onRowHasItem;
    public bool lastRow;
    public static bool LastRowBlocked;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RowComplete();
        
	}

    bool RowComplete()
    {
        
        List<RaycastHit2D> hits = new List<RaycastHit2D>(Physics2D.RaycastAll(transform.position, Vector2.right, 4.5f));

        if (lastRow)
        {
            bool temp = false;
            for (int i = 0; i < hits.Count; i++)
            {
                if (hits[i].collider.tag == "Block" /*&& hits[i].collider.tag != "Borders"*/)
                {
                    onRowHasItem.Invoke();
                    //print(name + ", " + hits[i].collider.name);
                    temp = true;
                    break;
                }

            }
            LastRowBlocked = temp;
            //print(temp);
        }
        hits.RemoveAll((x) => x.collider.tag != "Block");
        
        if (hits.Count == GameManager.Instance.gridDimentionX)
        {
            hits.Sort((x, y) => { return x.collider.transform.position.x.CompareTo(y.collider.transform.position.x); });
            for(int i=0;i<hits.Count;i++)
            {
                hits[i].collider.GetComponent<Block>().InitDestroy();
            }
            //foreach (RaycastHit2D hit in hits)
            //{
            //    hit.collider.GetComponent<Block>().InitDestroy();
            //}
            onRowComplete.Invoke();
            Block[] blocks = FindObjectsOfType<Block>();
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].transform.position.y - GameManager.Instance.blockSize / 2 > transform.position.y)
                    blocks[i].Invoke("MoveDown", .5f);//MoveDown();
            }
            return true;
        }
        return hits.Count == GameManager.Instance.gridDimentionX;

    }
}
