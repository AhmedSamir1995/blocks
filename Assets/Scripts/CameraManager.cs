﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
    public GameObject portraitCamera;
    public GameObject landscapeCamera;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        switch (Input.deviceOrientation)
        {
            case DeviceOrientation.Portrait:
            case DeviceOrientation.PortraitUpsideDown:
                portraitCamera.SetActive(true);
                landscapeCamera.SetActive(false);
                break;

            case DeviceOrientation.LandscapeLeft:
            case DeviceOrientation.LandscapeRight:
                landscapeCamera.SetActive(true);
                portraitCamera.SetActive(false);
                break;
        }
	}
    
}
