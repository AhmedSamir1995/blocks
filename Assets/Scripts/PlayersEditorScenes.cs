﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersEditorScenes : MonoBehaviour {
    public LeaderBoardAsset lb;

    private void Start()
    {

        lb = LeaderBoardAsset.GetLeaderBoardAsset();
        /*print(lb);
        if (lb == null)
            lb = new LeaderBoardAsset();*/
    }

    private void OnGUI()
    {



        // Show the custom GUI controls.

        GUILayout.BeginHorizontal();

        //EditorGUILayout.LabelField("First Name", EditorStyles.boldLabel, GUILayout.ExpandWidth(true));
        //EditorGUILayout.LabelField("Last Name", EditorStyles.boldLabel, GUILayout.ExpandWidth(true));
        //EditorGUILayout.LabelField("Score", EditorStyles.boldLabel, GUILayout.ExpandWidth(true));

        GUILayout.Label("First Name", GUILayout.Width(100));
        GUILayout.Label("Last Name", GUILayout.Width(100));
        GUILayout.Label("Score", GUILayout.Width(100));
        GUILayout.EndHorizontal();
        for (int i = 0; i < lb.playersData.Count; i++)
        {
            GUILayout.BeginHorizontal();
            lb.playersData[i].firstName=GUILayout.TextField(lb.playersData[i].firstName, GUILayout.Width(100));
            lb.playersData[i].lastName=GUILayout.TextField(lb.playersData[i].lastName, GUILayout.Width(100));
            string scoreString = GUILayout.TextField(lb.playersData[i].score.ToString(), GUILayout.Width(100));
            uint.TryParse(scoreString, out lb.playersData[i].score);
            if (GUILayout.Button("-"))
            {
                lb.playersData.RemoveAt(i);

            }
            GUILayout.EndHorizontal();
        }
        //LeaderBoardSaver.Save();

        if (GUILayout.Button("+"))
        {
            lb.playersData.Add(new PlayerData() { firstName = "First Name", lastName = "Last Name", score = 0 });
            

        }
        if (GUILayout.Button("Save"))
        {
            //lb.playersData.Add(new PlayerData() { firstName = "First Name", lastName = "Last Name", score = 0 });
            LeaderBoardSaver.Save();
        }
        if (GUILayout.Button("Quit"))
        {
            //lb.playersData.Add(new PlayerData() { firstName = "First Name", lastName = "Last Name", score = 0 });
            Application.Quit();
        }
    }
}
