﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    Block[,] grid;
    uint score=0;
    public uint rowScore;
    public GameObject groundedBlocks;
    public GameObject[] blocksPrefabs;
    public BlockAsset[] blocksData;
    public Queue<GameObject> upComing;
    public int upcomingLenth;
    public uint gridDimentionX;
    public uint gridDimentionY;
    public float blockSize;
    public int downSpeed;
    public float updatePeriod;
    public Vector2 min;
    public Vector2 max;
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }
    public UnityEngine.Events.UnityEvent onLose;
    public UnityEngine.UI.Text scoreText;
    [Multiline]
    public string congratulationText;
    public UnityEngine.UI.Text congratsScoreText;
    bool GamePlaying;
    public GameManager()
    {
        if (instance == null)
            instance = this;
    }
    // Use this for initialization
    void Start () {
        Input.backButtonLeavesApp=true;
        upComing = new Queue<GameObject>();
        grid = new Block[gridDimentionX,gridDimentionY];
        //print(grid.Length);
        Input.backButtonLeavesApp = true;
	}
	public void StartGame()
    {
        GamePlaying = true;
        score = 0;
        UpdateScore();
        InitializeQueue();
        Invoke("SpawnNewBlock", 1);
    }
    public void EndGame()
    {
        onLose.Invoke();
        congratsScoreText.text = congratulationText + score.ToString();
        simulator[] simulators = FindObjectsOfType<simulator>();
        for(int i=0;i<simulators.Length;i++)
        {
            Destroy(simulators[i].gameObject);
        }
        GamePlaying = false;
        DisposeQueue();
    }
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
            Quit();
	}

    public void Quit()
    {
        print("Quit");
        Application.Quit();
    }
    public void SetCell(int x, int y, Block block)
    {
        grid[x, y]=block;
    }
    [ContextMenu("New Block")]
    public GameObject SpawnNewBlock()
    {
        if (GamePlaying == false)
            return null;
        if (blocksData.Length == 0)
            return null;
        //print("Last Row " + Sensor.LastRowBlocked);
        if (Sensor.LastRowBlocked)
        {
            EndGame();
            return null;
        }
       /* Random.InitState((int)(System.DateTime.Now.Ticks%150));
        int i = Random.Range(0, blocksData.Length);
        */
        GameObject GO = GetNewBlock();
        GO.SetActive(true);
        //GO.transform.localScale += Vector3.left * Random.Range(0, 2)*2;
        return GO;
    }
    int lastRandomIndex;
    public GameObject CreateNewBlock()
    {
        if (blocksData.Length == 0)
            return null;

        int i ;
        do
        {
            i = Random.Range(0, blocksData.Length);
        }
        while (i == lastRandomIndex);
        lastRandomIndex = i;
        GameObject GO = GameObject.Instantiate(blocksData[i].blockPrefab);
        Vector3 scale = Vector3.left * Random.Range(0, 2) * 2;
        GO.transform.localScale +=  scale;
        //print(NextBlocksView.instance);
        NextBlocksView.instance.Add(blocksData[i].blockUIPrefab, scale);
        upComing.Enqueue(GO);
        print("queue" + upComing.Count);
        GO.SetActive(false);
        return GO;
    }
    public void InitializeQueue()
    {
        for (int i = 0; i < upcomingLenth; i++)
        {
            CreateNewBlock();
        }
    }
    public void DisposeQueue()
    {
        for(int i=0;0<upComing.Count;i++)
        {
            Destroy(upComing.Dequeue());
        }
        upComing.Clear();
        NextBlocksView.instance.Clear();
    }
    public GameObject GetNewBlock()
    {
        CreateNewBlock();
        if (upComing.Count > 0)
        {
            NextBlocksView.instance.Remove();
            return upComing.Dequeue();
        }
        else
            return null;
    }

    [ContextMenu("Increase Score")]
    public void IncreaseScore()
    {
        score+=rowScore;
        Invoke("UpdateScore", .2f);
    }

    public void UpdateScore()
    {
        scoreText.text = "SCORE: " + score.ToString();
    }

    public uint GetScore()
    {
        return score;
    }
    private void OnApplicationQuit()
    {

        LeaderBoardSaver.Save();
    }
}
