﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {
    public float timeInSecs;
    public UnityEngine.UI.Text timerText;
    public UnityEngine.UI.Image timerFillImage;
    public UnityEngine.Events.UnityEvent OnTimeChange;
    public UnityEngine.Events.UnityEvent OnTimeOut;
    public float currentTime;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        currentTime -= Time.deltaTime;
        /*if (currentTime>=0 && timerFillImage)
            timerFillImage.fillAmount = currentTime / timeInSecs;
            */
    }

    public void StartTimer()
    {
        currentTime = timeInSecs;
        InvokeRepeating("UpdateTimer",0,1);
    }
    public void UpdateTimer()
    {
        //currentTime -= 1;
        OnTimeChange.Invoke();
        if (timerText)
        {
            timerText.text = ((int)currentTime+1).ToString();
        }
        if (timerFillImage)
            timerFillImage.fillAmount = currentTime / timeInSecs;
        if((int)currentTime<0)
        {
            OnTimeOut.Invoke();
            StopTimer();
            return;
        }
    }

    public void StopTimer()
    {
        CancelInvoke("UpdateTimer");
    }
}
