﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LeaderBoard : MonoBehaviour {
    public GameObject leaderBoardEntryPrefab;
    public List<LeaderBoardEntry> Entries;
    public LeaderBoardAsset leaderBoardFile;
    public InputField firstNameInput;
    public InputField lastNameInput;

    // Use this for initialization
    void Start () {
        leaderBoardFile = LeaderBoardAsset.GetLeaderBoardAsset();
        ReadFileEntries();
        //LeaderBoardSaver.Load();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ReadFileEntries()
    {
        for(int i=0;i<leaderBoardFile.playersData.Count;i++)
        {
            /*LeaderBoardEntry entry = GameObject.Instantiate(leaderBoardEntryPrefab).GetComponent<LeaderBoardEntry>();
            if(entry)
            {*/
                CreateLeaderEntry(leaderBoardFile.playersData[i]);
            //}
        }
        //print("Reordering");
        ReorderChildren();
    }
    public LeaderBoardEntry CreateLeaderEntry(PlayerData playerData)
    {
        LeaderBoardEntry entry = GameObject.Instantiate(leaderBoardEntryPrefab,transform).GetComponent<LeaderBoardEntry>();
        transform.GetComponent<RectTransform>().sizeDelta += Vector2.up * 75;
        if (entry)
        {
            entry.SetPlayerData( playerData);
            Entries.Add(entry);
        }
        return entry;
    }
    public void AddPlayerToLeaderBoard(string firstName, string lastName, uint score)
    {

        //leaderBoardFile.playersData.Add(new PlayerData { firstName = firstName, lastName = lastName, score = score });
        PlayerData NewAddedPlayer = leaderBoardFile.AddEntry(firstName, lastName, score);
        if (NewAddedPlayer!=null)
            CreateLeaderEntry(NewAddedPlayer);
        ReorderChildren();

    }
    public void AddPlayerToLeaderBoard()
    {
        AddPlayerToLeaderBoard(firstNameInput.text, lastNameInput.text, GameManager.Instance.GetScore());

    }
    [ContextMenu("RemoveEntry")]
    public void RemovePlayerFromLeaderBoard()
    {
        leaderBoardFile.RemoveEntry(firstNameInput.text, lastNameInput.text, GameManager.Instance.GetScore());
    }

    public void ReorderChildren()
    {
        //print(Entries.Count);
        Entries.Sort((x,y)=> { if (x.playerData.score.CompareTo(y.playerData.score) == 0) return x.playerData.firstName.CompareTo(y.playerData.firstName); return -x.playerData.score.CompareTo(y.playerData.score); });
        for(int i=0;i<Entries.Count;i++)
        {
            Entries[i].transform.SetSiblingIndex(i);
            //print("Rank= " + i);
            Entries[i].Rank = i+1;
        }
    }
}
