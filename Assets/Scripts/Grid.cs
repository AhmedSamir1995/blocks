﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {
    public SpriteRenderer[] spriterenders;
    public Color color;
    public Color cellColor;
	// Use this for initialization
	void Start () {
        spriterenders = GetComponentsInChildren<SpriteRenderer>();
        //InvokeRepeating("ChangeGridColors", 0, .1f);
	}
	
	// Update is called once per frame
	void Update () {
		
        if(color!=cellColor)
        {
            ChangeGridColors(color);
            cellColor = color;
        }
	}

    public void ChangeGridColors(Color color)
    {
        Color temp = color;//Random.ColorHSV(0, 1, 1, 1, .2f, .2f);
        for(int i=0;i<spriterenders.Length;i++)
        {
            if (spriterenders[i])
            {
                spriterenders[i].color = temp;
            }
        }
    }
}
