﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SubmitButton : MonoBehaviour {
    public string firstName;
    public string lastName;
    public string FirstName
    {
        get
        {
            return firstName;
        }
        set
        {
            firstName = value;
            if (firstName != "" && lastName != "")
                this.GetComponent<Button>().interactable = true;
            else
                this.GetComponent<Button>().interactable = false;

        }
    }
    public string LastName
    {
        get
        {
            return lastName;
        }
        set
        {
            lastName = value;
            if (firstName != "" && lastName != "")
                this.GetComponent<Button>().interactable = true;
            else
                this.GetComponent<Button>().interactable = true;
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
