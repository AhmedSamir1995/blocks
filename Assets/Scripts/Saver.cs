﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class Saver
{
    public static string DefaultSaveLoadDirectory { set; get; }
    public static bool Save<D>(D Data, string fileName,string directoryPath)
    {
        if (!Directory.Exists(directoryPath))
            return false;
        FileStream FS;
        if (!File.Exists(directoryPath + "/" + fileName))
        {
            Debug.Log("Not found, creating file");
            FS = File.Create(directoryPath + "/" + fileName);
        }
        else
        {
            Debug.Log("file found");
            FS = File.Open(directoryPath + "/" + fileName, FileMode.Open);
        }
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(FS, Data);
        FS.Close();
        return true;
    }
    public static bool Load<D>(out D Data, string fileName,string directoryPath)
    {

        if (File.Exists(directoryPath + "/" + fileName))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream FS = File.Open(directoryPath + "/" + fileName, FileMode.Open);

            Data = (D)bf.Deserialize(FS);
            Debug.Log(Data);

            FS.Close();
            return true;
        }
        else
        {
            Debug.LogWarning("File Not exist");
            Data = default(D);
            return false;
        }
    }

    public static bool Save<D>(D Data, string fileName) 
    {
        return Save<D>(Data, fileName, DefaultSaveLoadDirectory);
    }
    public static bool Load<D>(out D Data, string fileName)
    {
        return Load<D>(out Data, fileName, DefaultSaveLoadDirectory);
    }

    static Saver()
    {

        Debug.Log("Data path: "+Application.dataPath);
        Debug.Log("Persistent Data path: "+Application.persistentDataPath);
        DefaultSaveLoadDirectory = Application.dataPath;
    }
}
