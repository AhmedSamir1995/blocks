﻿using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using UnityEngine;

public static class  LeaderBoardSaver {
    public static LeaderBoardAsset leaderboardData;
    const string folderName = "LeaderBoardData";
    const string fileExtension = ".lbdat";


    public static void Save()
    {

        Saver.Save<LeaderBoardAsset>(leaderboardData, "TetrisMO4HardLeaderboard.asset");
        //leaderboardData = UnityEngine.JsonUtility.FromJson<LeaderBoardAsset>();
        //PlayerPrefs.SetString("LeaderBoard", UnityEngine.JsonUtility.ToJson(leaderboardData));
    }

    public static void Load()
    {
        //  leaderboardData = UnityEngine.JsonUtility.FromJson<LeaderBoardAsset>(PlayerPrefs.GetString("LeaderBoard"));

        Saver.Load<LeaderBoardAsset>(out leaderboardData, "TetrisMO4HardLeaderboard.asset");
        //leaderboardData = UnityEngine.JsonUtility.FromJson<LeaderBoardAsset>(PlayerPrefs.GetString("LeaderBoard"));
        if (leaderboardData == null)
        {
            leaderboardData = new LeaderBoardAsset();

        }
        if (leaderboardData.playersData == null)
            leaderboardData.playersData = new List<PlayerData>();
    }
    static LeaderBoardSaver()
    {
        Load();
    }

    public static void Clear()
    {
        leaderboardData = new LeaderBoardAsset() { playersData = new List<PlayerData>() };
        Save();
    }
    /*
    public void Save()
    {
        FileStream fs = new FileStream("TetrisMO4Leaderboard.dat", FileMode.Create);
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(fs, leaderboardData);
        fs.Close();
        print("save");
        
        string folderPath = Path.Combine(Application.persistentDataPath, folderName);
        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);

        string dataPath = Path.Combine(folderPath, "TetrisMO4Leaderboard" + fileExtension);
        SaveLeaderboard(leaderboardData, dataPath);
    }
    public void Load()
    {
        print(leaderboardData);
        if (File.ex("TetrisMO4Leaderboard.dat", FileMode.Open))
        {
            leaderboardData = new LeaderBoardAsset() { playersData = new List<PlayerData>() };
        }
        else
        {
            using (Stream stream = File.Open("TetrisMO4Leaderboard.dat", FileMode.Open))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                leaderboardData = (LeaderBoardAsset)bformatter.Deserialize(stream);
            }
        }
        print("load");
        
        string[] filePaths = GetFilePaths();

        if (filePaths.Length > 0)
            leaderboardData = LoadLeaderboard(filePaths[0]);
    }
    static void SaveLeaderboard(LeaderBoardAsset data, string path)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();

        using (FileStream fileStream = File.Open(path, FileMode.OpenOrCreate))
        {
            binaryFormatter.Serialize(fileStream, data);
        }
    }

    static LeaderBoardAsset LoadLeaderboard(string path)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();

        using (FileStream fileStream = File.Open(path, FileMode.Open))
        {
            return (LeaderBoardAsset)binaryFormatter.Deserialize(fileStream);
        }
    }

    static string[] GetFilePaths()
    {
        string folderPath = Path.Combine(Application.persistentDataPath, folderName);

        return Directory.GetFiles(folderPath, fileExtension);
    }*/
}
