﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[CreateAssetMenu(fileName = "PlayerData", menuName = "PlayerDataList", order = 1)]
[System.Serializable]
public class LeaderBoardAsset 
{

    public List<PlayerData> playersData;

    //Add PlayerDataEntry
    public PlayerData AddEntry(string firstName, string lastName, uint score)
    {
        int playerIndex = playersData.FindIndex((x) => { return x.firstName == firstName && x.lastName == lastName; });

        if (playerIndex >= 0)//Player Exist
        {
            if(playersData[playerIndex].score<score)
                playersData[playerIndex].SetData(firstName, lastName, score);
            LeaderBoardSaver.Save();
            return null;
        }
        else//Player is new
        {
            playersData.Add(new PlayerData { firstName = firstName, lastName = lastName, score = score });
            playerIndex = playersData.Count - 1;
        }

        LeaderBoardSaver.Save();
        return playersData[playerIndex];
    }
    public void RemoveEntry(string firstName, string lastName, uint score)
    {
        int playerIndex = playersData.FindIndex((x) => { return x.firstName == firstName && x.lastName == lastName; });

        if (playerIndex >= 0)
            playersData.RemoveAt(playerIndex);
        LeaderBoardSaver.Save();
    }
    public static LeaderBoardAsset GetLeaderBoardAsset()
    {
        return LeaderBoardSaver.leaderboardData;
    }
}
[System.Serializable]
public class PlayerData
{
    public string firstName;
    public string lastName;
    public uint score;
    public PlayerData SetData(string firstName,string lastName, uint score)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.score = score;
        return this;
    }
}

