﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BlockData", menuName = "BlockData", order = 1)]
public class BlockAsset : ScriptableObject {
    public GameObject blockPrefab;
    public GameObject blockUIPrefab;
}
