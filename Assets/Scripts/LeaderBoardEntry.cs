﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LeaderBoardEntry : MonoBehaviour,IComparer<LeaderBoardEntry>, IComparer{
    int rank;
    public int Rank
    {
        get
        {
            return rank;
        }

        set
        {
            rank = value;
            rankText.text = rank.ToString();
            RefreshPlayerData();
        }
    }
    public PlayerData playerData;
    public Text rankText;
    public Text firstNameText;
    public Text lastNameText;
    public Text scoreText;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int Compare(LeaderBoardEntry x, LeaderBoardEntry y)
    {
        return x.playerData.score.CompareTo(y.playerData.score);
    }

    public void SetPlayerData(PlayerData playerData)
    {
        this.playerData = playerData;

        firstNameText.text = playerData.firstName.ToString();
        lastNameText.text = playerData.lastName.ToString();
        scoreText.text = playerData.score.ToString();
    }
    public void RefreshPlayerData()
    {
        firstNameText.text = playerData.firstName.ToString();
        lastNameText.text = playerData.lastName.ToString();
        scoreText.text = playerData.score.ToString();
    }

    public int Compare(object x, object y)
    {
        if (x is PlayerData && y is PlayerData)
        {
            return Compare(x, y);
        }
        else
        {
            print("compared to non player Data");
            return -100;
        }
    }
}
