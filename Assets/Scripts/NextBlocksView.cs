﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextBlocksView : MonoBehaviour {
    public List<GameObject> upcomings;
	// Use this for initialization
	void Start () {
        	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public static NextBlocksView instance;
    public NextBlocksView()
    {
        if (instance == null)
            instance = this;
    }
    public void Add(GameObject upcomingUI, Vector3 scale)
    {
        GameObject temp = GameObject.Instantiate(upcomingUI, transform);
        temp.transform.localScale += scale;
        upcomings.Add(temp);
        print("AddNewBlockToUpcomingView");
    }
    public void Remove()
    {
        print("Removing1Upcoming");
        if(transform.childCount>0)
            GameObject.Destroy(transform.GetChild(0).gameObject);
    }
    public void Clear()
    {
        for(int i=0;i<transform.childCount;i++)
        {
            print("remove" + transform.GetChild(i).gameObject);
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }
}
