﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {
    public Animator anim;
    public UnityEngine.Events.UnityEvent onDestroyed;
    SpriteRenderer spriteRenderer;
    Vector2 position;
    Vector2 Position { get { return position; } }
    public Vector2 hitpoint;
    bool grounded;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
	}
	

    // Update is called once per frame
    void Update () {
        position = transform.position;

            IsTouchingGround();
	}
    public void InitDestroy()
    {
        GameObject.Destroy(GetComponent<BoxCollider2D>());
        if(anim)
        {
            anim.SetTrigger("Destroy");
        }
        else
        Destroy();
    }
    public void Destroy()
    {
        onDestroyed.Invoke();
        GameObject.Destroy(gameObject);
    }
    [ContextMenu("MoveDown")]
    public void MoveDown()
    {
        /*RaycastHit2D hit;
        hit = Physics2D.Raycast(transform.position+Vector3.down*GameManager.Instance.blockSize, Vector2.down);
        if(hit)
        {
            float distance = simulator.SnapToGrid(-hit.distance, GameManager.Instance.blockSize);
            transform.position += (Vector3.up*distance);
        }*/
        transform.position -= Vector3.up*GameManager.Instance.blockSize;
    }

    public bool IsTouchingGround()
    {
        bool ground;
        ground = false;
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, Vector2.down, GameManager.Instance.blockSize);
        foreach(var hit in hits)
        {
            if (hit.collider.transform.parent != transform.parent)
            {
                //print(name+", "+hit.collider.name);
                ground = true;
                //print(ground);

            }
        }
        grounded = ground;
        GetComponentInParent<simulator>().groundIsDown = ground;
        return ground;
    }
    public bool CheckOverlap()
    {
        Vector2 boxSize = GetComponent<BoxCollider2D>().size * transform.localScale*.85f;
        List<Collider2D> overlapping = new List<Collider2D>(Physics2D.OverlapBoxAll(transform.position, boxSize, transform.rotation.eulerAngles.z));
        
        overlapping.RemoveAll((x) => x.gameObject.tag == "DescendingBlocks"||x.gameObject==gameObject);
        foreach (Collider2D collider in overlapping)
        { if (collider)
            {
                print(name + "Over Laping" + collider.name);
                return true;
            }
        }
        return false;
    }
}
