﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simulator : MonoBehaviour
{
    public Vector2 originalPosSnapped;
    public Vector2 originalPosRaw;
    public Vector2 deltaPos;
    public float blockSize;
    new Rigidbody2D rigidbody2D;
    public int downSpeed;
    public float updatePeriod;
    public float nextMove;
    public float time;
    public float offset;
    public bool useGravity { get; set; }
    public bool groundIsDown;
    public UnityEngine.Events.UnityEvent onGrounded;
    public ColorPalette colorPalette;
    // Use this for initialization
    void Start () {
        useGravity = true;
        rigidbody2D = GetComponent<Rigidbody2D>();
        //rigidbody2D.velocity = Vector2.up * -5;
        //InvokeRepeating("Simulate", 0,.25f);

        SpriteRenderer[] spriteRenderer = GetComponentsInChildren<SpriteRenderer>();
        Color temp = colorPalette.GetColor();
        foreach (SpriteRenderer sr in spriteRenderer)
        {
            sr.color = temp;
        }

        EnableGravity();
        onGrounded.AddListener(() => { /*print("grounded bsalam");*/ GameManager.Instance.Invoke("SpawnNewBlock",.1f); });
    }
	
	// Update is called once per frame
	void Update () {
        /* time = Time.time;
         if (nextMove < Time.time)
         {
             nextMove = Time.time + updatePeriod;
             if(useGravity)
                 rigidbody2D.MovePosition(new Vector2(rigidbody2D.transform.position.x,rigidbody2D.transform.position.y-blockSize*downSpeed));
         }*/
        /*if (Input.GetMouseButtonDown(0))
        {
            OnMouseDown();
        }
        if (Input.GetMouseButtonUp(0))
        {
            OnMouseUp();
        }
        if (Input.GetMouseButton(0))
        {
            OnMouseDrag();
        }*/

        rigidbody2D.velocity = Vector2.zero;
	}
    public void MoveDown()
    {
        Block[] childrenBlocks= GetComponentsInChildren<Block>();
        
        foreach(var block in childrenBlocks)
        {
            if (block.IsTouchingGround())
            {
                groundIsDown = true;
                break;
            }
        }
        if (groundIsDown)
        {
            SetGrounded();
        }
        else
            rigidbody2D.MovePosition(new Vector2(rigidbody2D.transform.position.x, rigidbody2D.transform.position.y - blockSize * downSpeed));
    }
    public void EnableGravity()
    {
        useGravity = true;
        InvokeRepeating("MoveDown",1,1);
        rigidbody2D.isKinematic = false;
    }
    public void DisableGravity()
    {
        useGravity = false;
        CancelInvoke("MoveDown");
        rigidbody2D.isKinematic = true;
    }
    void Simulate()
    {

        Physics2D.Simulate(0.1f);
    }
    private void OnMouseDown()
    {
        originalPosRaw = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        originalPosSnapped = SnapToGrid(originalPosRaw);
        deltaPos = (Vector2)rigidbody2D.transform.position  - originalPosSnapped;
    }
    private void OnMouseDrag()
    {
        if (!useGravity)
            return;
        //Debug.Log("Drag" + time);
        print(deltaPos.x + "," + deltaPos.y);
        Vector2 newPos;
        newPos.x = SnapToGrid(Camera.main.ScreenToWorldPoint(Input.mousePosition).x);
        newPos.y = SnapToGrid(Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

          newPos += deltaPos;
        //newPos += Vector2.one * offset;
        if (useGravity)
        {
            newPos.y = Mathf.Min(rigidbody2D.transform.position.y, newPos.y);
        }

        
        rigidbody2D.MovePosition(newPos
            );
            
    }

    float SnapToGrid(float x)
    {
        return SnapToGrid(x, blockSize);
    }
    public static float SnapToGrid(float x, float blockSize)
    {
        float temp;
        temp = Mathf.FloorToInt(x / blockSize) * blockSize;
        return temp;
    }
    Vector2 SnapToGrid(Vector2 x)
    {
        return SnapToGrid(x, blockSize);
    }

    public static Vector2 SnapToGrid(Vector2 x, float blockSize)
    {
        Vector2 temp = new Vector2()
        {
            x = SnapToGrid(x.x,blockSize),
            y = SnapToGrid(x.y, blockSize)
        };
        return temp;
    }
    private void OnMouseUp()
    {
        //print(Input.touchCount);
        //if(Input.touchCount>0)
        print((originalPosSnapped+deltaPos - (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition)).magnitude);
            if (/*Input.touches[0].tapCount==1||*/useGravity&&
            /*(originalPosSnapped+deltaPos-(Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition)).magnitude<(blockSize)*/
            originalPosSnapped+deltaPos==rigidbody2D.position)
            {
            print("Rotate");

            Rotate();
            //rigidbody2D.constraints=RigidbodyConstraints2D.FreezeRotation;
        }
    }
    public void Rotate()
    {
        bool canRotate=true;
        transform.Rotate(0, 0, 90);
        transform.position = (transform.position);
        List<Block> blocks;
        float movement=0;
        //check if rotated in place
        blocks = new List<Block>(GetComponentsInChildren<Block>());
        foreach(Block b in blocks)
        {
            if(b.CheckOverlap())
            {
                canRotate=false;
                break;
            }
        }
        print("Can Rotate= " + canRotate);
        if (canRotate)
            return;
        //check if rotated and moved right
        movement = blockSize;
        transform.position = ( transform.position + Vector3.right * movement);
        blocks = new List<Block>(GetComponentsInChildren<Block>());
        canRotate = true;
        foreach (Block b in blocks)
        {
            if (b.CheckOverlap())
            {
                canRotate=false;
                break;
            }
        }
        print("Can Rotate, right= " + canRotate);
        if (canRotate)
            return;

        //check if rotated and moved right 2
        movement = blockSize;
        transform.position = (transform.position + Vector3.right * movement);
        blocks = new List<Block>(GetComponentsInChildren<Block>());
        canRotate = true;
        foreach (Block b in blocks)
        {
            if (b.CheckOverlap())
            {
                canRotate = false;
                break;
            }
        }
        print("Can Rotate, right2= " + canRotate);
        if (canRotate)
            return;
        
        movement = -2*blockSize;
            transform.position = (transform.position + Vector3.right * movement);
        //check if rotated and moved left
        movement = -blockSize;
        transform.position = (transform.position + Vector3.right * movement);
        blocks = new List<Block>(GetComponentsInChildren<Block>());
        canRotate = true;
        foreach (Block b in blocks)
        {
            if (b.CheckOverlap())
            {
                canRotate=false;
                break;
            }
        }
        print("Can Rotate, left= " + canRotate);
        if (canRotate)
            return;

        //check if rotated and moved left 2
        movement = -blockSize;
        transform.position = (transform.position + Vector3.right * movement);
        blocks = new List<Block>(GetComponentsInChildren<Block>());
        canRotate = true;
        foreach (Block b in blocks)
        {
            if (b.CheckOverlap())
            {
                canRotate = false;
                break;
            }
        }
        print("Can Rotate, left2= " + canRotate);
        if (canRotate)
            return;


        if (!canRotate)
        {

            movement = 2*blockSize;
            transform.position = (transform.position + Vector3.right * movement);
            transform.Rotate(0, 0, -90);
        }
    }
    public void SetGrounded()
    {
        rigidbody2D.MovePosition(SnapToGrid((Vector2)transform.position));
        if (useGravity == false)
            return;
        {
            //print("Ayy"+gameObject.name +", " + collision.collider.name +":"+ collision.collider.tag);
            DisableGravity();
                //tag = "Ground";
                foreach (var go in transform.GetComponentsInChildren<Transform>())
                {
                    go.tag = tag;
                }
                downSpeed = 0;
            onGrounded.Invoke();
        }
    }
}
